<?php

namespace idfortysix\translationdownloader;

use Illuminate\Support\ServiceProvider;

class TranslationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                DownloadTrans::class,
            ]);
    	}
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        
    }
}

